from string import ascii_lowercase
from copy import copy

PATH_TO_VOTES = "/Users/nico/Desktop/KnessetVotes/votes.txt"
TOTAL_SEATS = 120


def load_votes(path_to_vote: str):
    """
    load the votes located in text file
    :param path_to_vote: absolute path to vote
    :return: the votes in a string variable
    """
    with open(path_to_vote, 'r') as file:
        votes = file.read()
    return votes


def make_vote_dictionary(votes: str):
    """
    Create and return a vote dictionary to establish which party won
    :param votes: a string containing all votes
    :return: the vote dictionary
    """
    vote_dictionary = {}
    correct_vote_counter = 0
    for vote in votes:
        if vote in ascii_lowercase:
            if vote in vote_dictionary:
                vote_dictionary[vote] += 1
            else:
                vote_dictionary[vote] = 1
            correct_vote_counter += 1

    return vote_dictionary, correct_vote_counter, len(votes) - correct_vote_counter, len(votes)


def add_percentage(vote_dictionary: dict):
    """
    Add the percentage info to the vote dict
    :param vote_dictionary: the dictionary that counts the number of votes for each party
    :return: the modified dictionary
    """
    number_of_correct_votes = 0
    for party in vote_dictionary:
        number_of_correct_votes += vote_dictionary[party]

    for party in vote_dictionary:
        vote_dictionary[party] = [vote_dictionary[party], vote_dictionary[party] / number_of_correct_votes]

    return vote_dictionary


def print_election_header():
    """
    Display a nice header to announce results
    """
    print("Election Results")
    print("=" * 10)
    print("\n")


def print_percentage(vote_dictionary: dict):
    """
    print the number of votes and percentage it represents for each party
    also remove parties that have not enough votes
    :param vote_dictionary: the dictionary that counts the number of votes for each party
    """
    updated_dictionary = {}
    for party in vote_dictionary:
        if vote_dictionary[party][1] < 0.035:
            print("Party %s : got %d votes or %f percent >>> less than 3.5 percent"
                  % (party, vote_dictionary[party][0], vote_dictionary[party][1] * 100))
        else:
            print("Party %s : got %d votes or %f percent"
                  % (party, vote_dictionary[party][0], vote_dictionary[party][1] * 100))
            updated_dictionary[party] = copy(vote_dictionary[party])

    # update the vote dictionary
    vote_dictionary = updated_dictionary
    return vote_dictionary


def print_election_results(vote_dictionary: dict):
    """
    print the attribution of Knesset seats
    :param vote_dictionary: dictionary counting votes
    """
    for party in vote_dictionary:
        print("Party %s : got %d votes or %f percent and will have %d members"
              % (party, vote_dictionary[party][0],
                 vote_dictionary[party][1] * 100,
                 int(vote_dictionary[party][1] * 120))
              )


def print_vote_count(good_votes, bad_votes, total_votes):
    """
    print the current vote count
    :param good_votes: number of good_votes
    :param bad_votes: number of bad_votes
    :param total_votes: total number of votes
    """
    print("good votes %d\nbad votes %d\ntotal votes %d\n" % (good_votes, bad_votes, total_votes))


def main():

    print_election_header()

    votes = load_votes(PATH_TO_VOTES)

    vote_dictionary, good_votes, bad_votes, total_votes = make_vote_dictionary(votes)
    vote_dictionary = add_percentage(vote_dictionary)

    print_vote_count(good_votes, bad_votes, total_votes)
    vote_dictionary = print_percentage(vote_dictionary)

    good_votes = 0
    for party in vote_dictionary:
        good_votes += vote_dictionary[party][0]

    total_votes = good_votes + bad_votes

    print("After eliminating parties with less than 3.5 %\n")
    print_vote_count(good_votes, bad_votes, total_votes)
    print_election_results(vote_dictionary)


if __name__ == "__main__":
    main()
